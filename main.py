MAX_NUMBER_TO_CHECK = 8128

divisors_sum = 0

for i in range(1, MAX_NUMBER_TO_CHECK + 1):
    for j in range(1, i):
        if i % j == 0:
            divisors_sum += j
    
    if divisors_sum == i:
        print(i)
        
    divisors_sum = 0
